# Homework1 &mdash; Unit Testing with JUnit 5

## Objective

Practice writing JUnit 5 test cases.

## Instructions

* You must fork the Unit Testing Homework repository to your private GitLab subgroup under the class subgroup. It will look something like `Worcester State University / Computer Science Department / CS 443 01 02 Spring 2024 / Students/ username`
* All of your tests are to be written to test the Order System code in
[src/main/java/edu/worcester/cs](src/main/java/edu/worcester/cs).
* You must create one test class for each of the three classes in the Order System.
  * Test classes must be written in [src/test/java/edu/worcester/cs](src/main/test/edu/worcester/cs)
  * Your tests must be written using JUnit 5.
  * Your tests must be named to clearly explain what the method is testing.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Write tests for the following

* Product Class
  * Constructing a Product with a non-negative price
  * Getting and setting non-negative prices
  * Getting and setting sku
  * Getting and setting description
* Customer Class
  * Constructing a Customer
  * Getting and setting first and last names
  * Getting customer number
* Order Class
  * Constructing an Order
  * Adding a product with non-negative quantity to the order
  * Removing a product with a non-negative quantity from the order
  * Checking if a product is in the order
  * Checking the quantity of a product in the order
  * Getting the total price of the order
  * Getting the number of unique products in the order
  * Getting the total number of items in the order

## *Intermediate Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Write tests for the following

* Product Class
  * Constructing a Product with a negative price
  * Getting and setting negative prices
  * equals()
* Customer Class
  * equals()
* Order Class
  * Adding a product with a negative quantity to the order
  * Removing a product with a negative quantity from the order
  * Adding a new product to the order (not already in the order)
  * Removing a product that is not in the order
  * equals()

## *Advanced Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Advanced Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Write tests for the following

* Product Class
  * toString()
* Customer Class
  * Checking that the customer number auto-increments with each new customer object created
  * toString()
* Order Class
  * Checking that the order number auto-increments with each new order object created
  * Adding a product that is already in the order
  * Removing a product that is in the order - quantity less than the quantity in the order
  * Removing a product that is in the order - quantity that is greater than or equal to the quantity in the order
  * toString()

## Deliverables

You must add, commit, and push your project containing your test classes to GitLab.

## Due Date

* Wednesday, 14 February 2025 - 23:59

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
